import { Component, OnInit } from '@angular/core';
import {LoadingOverlayService} from "../shared/loading-overlay.service";

@Component({
  selector: 'app-loading-overlay',
  templateUrl: './loading-overlay.component.html',
  styleUrls: ['./loading-overlay.component.less']
})
export class LoadingOverlayComponent implements OnInit {

  constructor(private overlayService: LoadingOverlayService) { }

  ngOnInit() {
  }

  isInProgress() {
    return this.overlayService.isInProgress();
  }
}
