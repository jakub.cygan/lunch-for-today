import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAccountPageComponent } from './my-account-page.component';
import {RouterTestingModule} from "@angular/router/testing";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatListModule} from "@angular/material/list";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatTableModule} from "@angular/material/table";
import {MatSliderModule} from "@angular/material/slider";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatBadgeModule} from "@angular/material/badge";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatStepperModule} from "@angular/material/stepper";
import {MatDialogModule} from "@angular/material/dialog";
import {AuthService} from "../shared/auth.service";
import {Subject} from "rxjs";

describe('MyAccountPageComponent', () => {
  let component: MyAccountPageComponent;
  let fixture: ComponentFixture<MyAccountPageComponent>;

  beforeEach(async(() => {
    const spy = jasmine.createSpyObj('AuthService', ['create']);
    spy.user = new Subject();

    TestBed.configureTestingModule({

      imports: [
        RouterTestingModule,
        MatSidenavModule,
        MatToolbarModule,
        MatIconModule,
        MatListModule,
        MatButtonModule,
        MatCardModule,
        MatTableModule,
        MatSliderModule,
        FormsModule,
        MatFormFieldModule,
        MatBadgeModule,
        BrowserAnimationsModule,
        MatStepperModule,
        ReactiveFormsModule,
        MatDialogModule
      ],
      declarations: [ MyAccountPageComponent],
      providers: [
        {provide: AuthService, useValue: spy},]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyAccountPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
