import {Component, OnInit} from '@angular/core';
import {AuthService} from "../shared/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-my-account-page',
  templateUrl: './my-account-page.component.html',
  styleUrls: ['./my-account-page.component.less']
})
export class MyAccountPageComponent implements OnInit {
  userName: String;

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.userName = this.authService.getCurrentUser().user.displayName;
  }

  logout() {
    this.authService.signOut();
    this.router.navigate([""]);
  }
}
