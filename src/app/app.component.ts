import {Component, OnInit} from '@angular/core';
import * as firebase from 'firebase/app'
import {FirebaseApp} from "@angular/fire";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title = 'lunch-for-today';

  constructor(private app: FirebaseApp) {
  }

  ngOnInit() {
    this.app.auth().app.analytics();
  }
}
