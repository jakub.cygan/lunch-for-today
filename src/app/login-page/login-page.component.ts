import {Component, OnInit} from '@angular/core';
import {AuthService} from "../shared/auth.service";
import {Router} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.less']
})
export class LoginPageComponent implements OnInit {

  constructor(private fireAuth: AuthService,
              private router: Router,
              private location: Location) {
  }

  ngOnInit() {
    this.fireAuth.subscribeOnUser(user => {
      this.location.back();
    });
  }

  doLogin(): void {
    this.fireAuth.signInWithGoogle()
      .then(result => console.log(result))
      .catch(error => console.log(error));
  }

}
