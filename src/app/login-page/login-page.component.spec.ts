import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginPageComponent } from './login-page.component';
import {BehaviorSubject, Subject} from "rxjs";
import {AuthService} from "../shared/auth.service";
import {Location} from "@angular/common";

describe('LoginPageComponent', () => {
  let component: LoginPageComponent;
  let fixture: ComponentFixture<LoginPageComponent>;

  beforeEach(async(() => {
    const spy = jasmine.createSpyObj('AuthService', ['create']);
    spy.user = new Subject();

    TestBed.configureTestingModule({
      declarations: [ LoginPageComponent ],
      providers: [
        {provide: AuthService, useValue: spy},
        {provide: Location, useValue: jasmine.createSpyObj('Location', ['back'])},
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


const FirestoreStub = {
  collection: (name: string) => ({
    doc: (_id: string) => ({
      valueChanges: () => new BehaviorSubject({foo: 'bar'}),
      set: (_d: any) => new Promise((resolve, _reject) => resolve()),
    }),
  })
};
