import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Vote} from '../shared/vote';
import {VotesService} from '../shared/votes.service';
import {firestore} from 'firebase';
import {Result} from './shared/result';
import {VoteCandidateResult} from '../shared/vote-candidate';
import Timestamp = firestore.Timestamp;

@Component({
  selector: 'app-vote-results-page',
  templateUrl: './vote-results-page.component.html',
  styleUrls: ['./vote-results-page.component.less']
})
export class VoteResultsPageComponent implements OnInit {
  vote: Vote;
  resultItems: Result[] = [];

  constructor(private route: ActivatedRoute,
              private votesService: VotesService) {
  }

  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      this.votesService.getById(routeParams.id).subscribe(vote => {
        this.processVote(vote, routeParams.id);
      });
    });
  }

  isBeforeDueDate() {
    if (this.vote) {
      return this.vote.dueDate > new Date();
    } else {
      return false;
    }
  }

  private processVote(vote, voteId) {
    this.vote = vote;
    const dueDateTimestamp = vote.dueDate as unknown as Timestamp;
    this.vote.dueDate = dueDateTimestamp.toDate();

    this.votesService.getVoteCandidates(this.vote.id).subscribe(candidates => {
      this.initializeResults(candidates);
    });

    this.votesService.getResults(voteId).subscribe(results => {
      this.cleanUpResultsValues();
      this.processResults(results).then(() => this.resultItems.sort((a, b) => (a.result > b.result) ? -1 : 1));
    });
  }

  private cleanUpResultsValues() {
    this.resultItems.forEach(result => {
      result.result = 0;
      result.yes = 0;
      result.no = 0;
      result.maybe = 0;
    });
  }

  private initializeResults(candidates) {
    candidates.forEach(candidate => this.resultItems.push({
      candidate,
      yes: 0,
      no: 0,
      maybe: 0,
      result: 0
    }));
  }

  private async processResults(results) {
    await results.forEach(result => {
      this.resultItems.forEach(item => {
        console.log(item);
        console.log(result);
        result.results
          .filter(voteResult => item.candidate.id === voteResult.candidateId)
          .forEach(candidateResult => {
            this.updateResultItemValues(item, candidateResult);
          });
      });
    });
  }

  private updateResultItemValues(item, candidateResult) {
    item.result += candidateResult.result;
    if (candidateResult.result === VoteCandidateResult.YES) {
      item.yes++;
    } else if (candidateResult.result === VoteCandidateResult.NO) {
      item.no++;
    } else if (candidateResult.result === VoteCandidateResult.MAYBE) {
      item.maybe++;
    }
  }
}
