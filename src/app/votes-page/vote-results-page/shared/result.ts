import {VoteCandidate} from '../../shared/vote-candidate';

export interface Result {
  yes: number;
  no: number;
  maybe: number;
  result: number;
  candidate: VoteCandidate;
}
