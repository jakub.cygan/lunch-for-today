import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoteResultsPageComponent } from './vote-results-page.component';

describe('VoteResultsPageComponent', () => {
  let component: VoteResultsPageComponent;
  let fixture: ComponentFixture<VoteResultsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoteResultsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoteResultsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
