import {Component, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {VotesService} from '../shared/votes.service';
import {VoteCandidate} from '../shared/vote-candidate';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Vote} from '../shared/vote';
import Timestamp = firebase.firestore.Timestamp;
import * as firebase from 'firebase';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.less']
})
export class VoteComponent {
  vote: Vote;
  displayedColumns = ['name', 'voting'];
  dataSource: MatTableDataSource<VoteCandidate>;
  candidates: VoteCandidate[];

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private route: ActivatedRoute,
              private votesService: VotesService,
              private router: Router,
              private snackBar: MatSnackBar) {
    this.route.params.subscribe(routeParams => {
      this.votesService.getById(routeParams.id).subscribe(vote => {
        this.vote = vote;
        const dueDateTimestamp = vote.dueDate as unknown as Timestamp;
        this.vote.dueDate = dueDateTimestamp.toDate();
      });

      this.candidates = [];
      this.votesService.getVoteCandidates(routeParams.id).subscribe(candidates => {
        this.dataSource = new MatTableDataSource<VoteCandidate>(candidates);
        this.dataSource.sort = this.sort;

        this.votesService.getResult(routeParams.id).subscribe(voteResult => {
          if (voteResult) {
            voteResult.results.forEach(result => this.dataSource.data
              .filter(data => data.id === result.candidateId)
              .pop()
              .result = result.result);
          }
        });
      });
    });
  }

  trackById(index, element: VoteCandidate) {
    return this.dataSource.data[index].id;
  }

  change(id: string, value: number) {
    // TODO: save results in separated model fit to save on server side
    this.dataSource.data.filter(element => element.id === id).pop().result = value;
  }

  submit() {
    const results = this.dataSource.data.map(element => {
      return {
        candidateId: element.id,
        result: element.result
      };
    });

    const allSelected = results
      .map(result => result.result !== null)
      .reduce((a, b) => a && b);
    if (!allSelected) {
      this.snackBar.open('You have to make your choose for all options', 'close');
      return;
    }

    this.votesService.saveResult(this.vote.id, results);
    this.goToResults();
  }

  goToResults() {
    this.router.navigate(['vote', this.vote.id, 'results']);
  }

  isBeforeDueDate() {
    if (this.vote) {
      return this.vote.dueDate > new Date();
    } else {
      return false;
    }
  }

  amIOwner() {
    return this.votesService.amIOwner(this.vote);
  }

  finisVote() {
    this.votesService.finish(this.vote.id);
  }
}
