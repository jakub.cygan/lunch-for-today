import {Component, OnInit} from '@angular/core';
import {VoteListItem} from "../shared/vote-list-item";
import {VotesService} from "../shared/votes.service";
import {LoadingOverlayService} from "../../shared/loading-overlay.service";

@Component({
  selector: 'app-votes-list',
  templateUrl: './votes-list.component.html',
  styleUrls: ['./votes-list.component.less']
})
export class VotesListComponent implements OnInit {

  votes: VoteListItem[];

  constructor(private votesService: VotesService,
              private loadingService: LoadingOverlayService) {
  }

  ngOnInit() {
    this.loadingService.start();
    this.votesService.getVotes().subscribe(votes => this.votes = votes);
    this.loadingService.finish();
  }

}
