import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {VotesListComponent} from './votes-list.component';
import {RouterTestingModule} from "@angular/router/testing";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatListModule} from "@angular/material/list";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatTableModule} from "@angular/material/table";
import {MatSliderModule} from "@angular/material/slider";
import {FormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatBadgeModule} from "@angular/material/badge";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {VotesService} from "../shared/votes.service";
import {Subject} from "rxjs";

describe('VotesListComponent', () => {
  let component: VotesListComponent;
  let fixture: ComponentFixture<VotesListComponent>;

  beforeEach(async(() => {

    const spy = jasmine.createSpyObj('VotesService', ['getVotes']);
    spy.getVotes.and.returnValue(new Subject());

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatSidenavModule,
        MatToolbarModule,
        MatIconModule,
        MatListModule,
        MatButtonModule,
        MatCardModule,
        MatTableModule,
        MatSliderModule,
        FormsModule,
        MatFormFieldModule,
        MatBadgeModule,
        BrowserAnimationsModule
      ],
      declarations: [VotesListComponent],
      providers: [
        {provide: VotesService, useValue: spy},]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VotesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
