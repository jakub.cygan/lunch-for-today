import {Injectable} from '@angular/core';
import {v4 as uuid} from 'uuid';
import {VoteListItem} from "./vote-list-item";
import {Vote} from "./vote";
import {VoteCandidate, VoteCandidateResult} from "./vote-candidate";
import {AngularFirestore} from "@angular/fire/firestore";
import {Observable} from "rxjs";
import {AuthService} from "../../shared/auth.service";
import {CandidateResult} from "./candidate-result";
import {VoteResult} from "./vote-result";
import {MatSnackBar} from "@angular/material/snack-bar";
import {LoadingOverlayService} from "../../shared/loading-overlay.service";

@Injectable({
  providedIn: 'root'
})
export class VotesService {

  constructor(private db: AngularFirestore,
              private authService: AuthService,
              private matSnackBar: MatSnackBar,
              private loadingService: LoadingOverlayService) {
  }

  getVotes(): Observable<VoteListItem[]> {
    let userId = this.authService.getCurrentUser().user.uid;
    return this.db
      .collection<VoteListItem>("/votes",
        query => query.where("ownerId", "==", userId))
      .valueChanges();
  }

  getById(id: string): Observable<Vote> {
    return this.db.doc<Vote>("/votes/" + id).valueChanges();
  }

  getVoteCandidates(id: string): Observable<VoteCandidate[]> {
    return this.db.doc("/votes/" + id)
      .collection<VoteCandidate>("/candidates").valueChanges();
  }

  create(name: string, description: string, dueDate: Date, candidates: VoteCandidate[]) {
    let newVote: Vote = {
      id: uuid.v4(),
      name: name,
      details: description,
      dueDate: dueDate,
      votePositions: null,
      ownerId: this.authService.getCurrentUser().user.uid
    };
    let batch = this.db.firestore.batch();

    let newVoteRef = this.db.firestore.collection("/votes").doc(newVote.id);
    batch.set(newVoteRef, newVote);

    let newVoteCandidatesRef = this.db.firestore.collection("/votes")
      .doc(newVote.id)
      .collection("/candidates");
    candidates.forEach(newCandidate =>
      batch.set(newVoteCandidatesRef.doc(newCandidate.id), newCandidate));

    batch.commit()
      .then(result => console.log(result))
      .catch(error => console.log(error));
  }

  getResult(voteId: string): Observable<VoteResult> {
    return this.db.collection("/votes")
      .doc(voteId)
      .collection("/results")
      .doc<VoteResult>(this.authService.getCurrentUser().user.uid)
      .valueChanges();
  }

  getResults(voteId: string): Observable<VoteResult[]> {
    return this.db.collection("/votes")
      .doc(voteId)
      .collection<VoteResult>("/results")
      .valueChanges();
  }

  saveResult(voteId: string, results: CandidateResult[]) {
    this.loadingService.start();
    let voteResult: VoteResult = {
      voteId: voteId,
      ownerId: this.authService.getCurrentUser().user.uid,
      results: results
    };
    this.db.collection("/votes")
      .doc(voteId)
      .collection("/results")
      .doc(voteResult.ownerId)
      .set(voteResult)
      .then(result => this.matSnackBar.open('Thanks for voting', "", {duration: 2000}))
      .catch(error => this.matSnackBar.open(error))
      .finally(() => this.loadingService.finish());

  }

  amIOwner(vote: Vote) {
    return this.authService.getCurrentUser().user.uid === vote.ownerId;
  }

  finish(id: string) {
    this.loadingService.start();
    this.db.collection("/votes")
      .doc<Vote>(id)
      .update({dueDate: new Date()})
      .then(result => this.matSnackBar.open('Finished', "", {duration: 2000}))
      .catch(error => this.matSnackBar.open(error))
      .finally(() => this.loadingService.finish());
  }
}
