import {CandidateResult} from "./candidate-result";

export interface VoteResult {
  voteId: string;
  ownerId: string;
  results: CandidateResult[];
}
