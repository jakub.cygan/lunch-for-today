import {VoteCandidateResult} from "./vote-candidate";

export interface CandidateResult {
  candidateId: string;
  result: VoteCandidateResult;
}
