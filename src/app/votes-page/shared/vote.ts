export interface Vote {
  id: string;
  ownerId: String;
  name: string
  details: string;
  dueDate: Date;
  votePositions: VotePosition[];
}
export interface VotePosition{
  name: string
  description: string;
  //TODO: add location etc
}
