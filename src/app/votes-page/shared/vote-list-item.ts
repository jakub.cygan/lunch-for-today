export interface VoteListItem {
  id: string;
  name: string;
  details: string;
}
