export interface VoteCandidate {
  id: string
  name: string
  rating: number,
  result: VoteCandidateResult
}

export enum VoteCandidateResult {
  YES = 1, NO = -1, MAYBE = 0
}
