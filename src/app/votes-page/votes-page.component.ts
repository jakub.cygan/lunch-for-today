import {Component} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {NewVoteDialogComponent} from "./new-vote-dialog/new-vote-dialog.component";
import {analytics} from "firebase";

@Component({
  selector: 'app-votes-page',
  templateUrl: './votes-page.component.html',
  styleUrls: ['./votes-page.component.less']
})
export class VotesPageComponent {


  constructor(private dialog: MatDialog) {
    analytics().logEvent('open votes', {value1: "test"});
  }

  openNewVoteDialog() {
    const newDialog = this.dialog.open(NewVoteDialogComponent);
    newDialog.afterClosed().subscribe(result => {
      console.log(result);
    });
  }
}
