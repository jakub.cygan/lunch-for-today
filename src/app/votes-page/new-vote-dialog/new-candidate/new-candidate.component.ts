import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {VoteCandidate, VoteCandidateResult} from "../../shared/vote-candidate";
import {v4 as uuid} from 'uuid';

@Component({
  selector: 'app-new-candidate',
  templateUrl: './new-candidate.component.html',
  styleUrls: ['./new-candidate.component.less']
})
export class NewCandidateComponent {
  newCandidate = {
    name: ''
  };

  @Input()
  candidates: VoteCandidate[];

  @Output()
  candidatesChange = new EventEmitter<VoteCandidate[]>();

  addCandidate() {
    this.candidates.push({
      id: uuid.v4(),
      name: this.newCandidate.name,
      rating: null,
      result: null
    });
    this.newCandidate = {
      name: ''
    };

    this.candidatesChange.emit(this.candidates);
  }

}
