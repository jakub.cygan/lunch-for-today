import {Component, OnInit} from '@angular/core';
import {VotesService} from '../shared/votes.service';
import {MatDialogRef} from '@angular/material/dialog';
import {VoteCandidate} from '../shared/vote-candidate';
import {FormBuilder, Validators} from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-new-vote-dialog',
  templateUrl: './new-vote-dialog.component.html',
  styleUrls: ['./new-vote-dialog.component.less']
})
export class NewVoteDialogComponent implements OnInit {

  firstFormGroup: any;
  secondFormGroup: any;
  candidates: VoteCandidate[];

  constructor(private votesService: VotesService,
              private dialogRef: MatDialogRef<void>,
              private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.candidates = [];
    this.firstFormGroup = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      dueTime: ['', Validators.required]
    });
    this.secondFormGroup = this.formBuilder.group({
    });
  }

  create() {
    const now = new Date();
    const dueTime = moment(this.firstFormGroup.value.dueTime, 'HH:mm');
    const dueDate = new Date(now.getFullYear(), now.getMonth(), now.getDate(), dueTime.hour(), dueTime.minute());
    this.votesService.create(
      this.firstFormGroup.value.name,
      this.firstFormGroup.value.description,
      dueDate,
      this.candidates);
    this.dialogRef.close();
  }

}
