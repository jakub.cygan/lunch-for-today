import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewVoteDialogComponent } from './new-vote-dialog.component';
import {RouterTestingModule} from "@angular/router/testing";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatListModule} from "@angular/material/list";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatTableModule} from "@angular/material/table";
import {MatSliderModule} from "@angular/material/slider";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatBadgeModule} from "@angular/material/badge";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatStepperModule} from "@angular/material/stepper";
import {Component, Input} from "@angular/core";
import {VoteCandidate} from "../shared/vote-candidate";
import {VotesService} from "../shared/votes.service";
import {MatDialogModule} from "@angular/material/dialog";

describe('NewVoteDialogComponent', () => {
  let component: NewVoteDialogComponent;
  let fixture: ComponentFixture<NewVoteDialogComponent>;

  beforeEach(async(() => {
    const spy = jasmine.createSpyObj('VotesService', ['create']);

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatSidenavModule,
        MatToolbarModule,
        MatIconModule,
        MatListModule,
        MatButtonModule,
        MatCardModule,
        MatTableModule,
        MatSliderModule,
        FormsModule,
        MatFormFieldModule,
        MatBadgeModule,
        BrowserAnimationsModule,
        MatStepperModule,
        ReactiveFormsModule,
        MatDialogModule
      ],
      declarations: [ NewVoteDialogComponent, MockCandidatesListComponent, MockNewCandidateComponent ],
      providers: [
        {provide: VotesService, useValue: spy},]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewVoteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


@Component({
  selector: 'app-candidates-list',
  template: ''
})
class MockCandidatesListComponent {

  @Input()
  candidates: VoteCandidate[];
}


@Component({
  selector: 'app-new-candidate',
  template: ''
})
class MockNewCandidateComponent {

  @Input()
  candidates: VoteCandidate[];
}
