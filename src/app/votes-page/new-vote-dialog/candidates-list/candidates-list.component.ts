import {Component, Input, OnInit} from '@angular/core';
import {VoteCandidate} from "../../shared/vote-candidate";

@Component({
  selector: 'app-candidates-list',
  templateUrl: './candidates-list.component.html',
  styleUrls: ['./candidates-list.component.less']
})
export class CandidatesListComponent implements OnInit {

  @Input()
  candidates: VoteCandidate[];

  constructor() {
  }

  ngOnInit() {
  }

  remove(index: number) {
    this.candidates.splice(index, 1);
  }
}
