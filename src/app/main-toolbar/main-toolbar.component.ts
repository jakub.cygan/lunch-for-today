import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-main-toolbar',
  templateUrl: './main-toolbar.component.html',
  styleUrls: ['./main-toolbar.component.less']
})
export class MainToolbarComponent implements OnInit {

  @Output()
  toggleSidebar = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
