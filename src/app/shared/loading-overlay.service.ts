import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoadingOverlayService {

  inProgressCounter: number = 0;

  constructor() {
  }

  start() {
    this.inProgressCounter++;
  }

  finish() {
    this.inProgressCounter--;
    if (this.inProgressCounter < 0) {
      this.inProgressCounter = 0;
    }
  }

  isInProgress() {
    return this.inProgressCounter > 0;
  }
}
