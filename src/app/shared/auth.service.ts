import {Injectable} from '@angular/core';
import {Subject} from "rxjs";
import {AngularFireAuth} from "@angular/fire/auth";
import * as firebase from 'firebase/app';
import {User} from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly userSubject: Subject<UserProfile> = new Subject();

  private currentUser: UserProfile;

  constructor(public readonly firebaseAuth: AngularFireAuth) {
    this.firebaseAuth.user.subscribe(user => {
      if (!user) {
        return;
      }
      this.populateUser(user);
    });
  }

  private populateUser(user) {
    let userProfile = {
      user: user,
      additionalInfo: null
    };
    this.currentUser = userProfile;
    this.userSubject.next(userProfile);
  }

  signInWithGoogle() {
    return this.firebaseAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }

  subscribeOnUser(subscription) {
    return this.userSubject.subscribe(subscription);
  }

  getCurrentUser(): UserProfile{
    return this.currentUser;
  }

  signOut() {
    this.firebaseAuth.auth.signOut();
  }
}

export interface UserAdditionalInfo {
  name: string;
  role: string,
  uid: string
}

export interface UserProfile {
  user: User;
  additionalInfo: UserAdditionalInfo;
}
