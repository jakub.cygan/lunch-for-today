import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {VotesPageComponent} from './votes-page/votes-page.component';
import {VoteComponent} from "./votes-page/vote/vote.component";
import {LoginPageComponent} from "./login-page/login-page.component";
import {MyAccountPageComponent} from "./my-account-page/my-account-page.component";
import {
  AngularFireAuthGuard,
  redirectUnauthorizedTo
} from "@angular/fire/auth-guard";
import {VoteResultsPageComponent} from "./votes-page/vote-results-page/vote-results-page.component";

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);

const routes: Routes = [
  {path: '', redirectTo: 'votes', pathMatch: 'full'},
  {path: 'login', component: LoginPageComponent},
  {path: 'votes', component: VotesPageComponent, canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin }},
  {path: 'vote/:id', component: VoteComponent, canActivate: [AngularFireAuthGuard],  data: { authGuardPipe: redirectUnauthorizedToLogin }},
  {path: 'vote/:id/results', component: VoteResultsPageComponent, canActivate: [AngularFireAuthGuard],  data: { authGuardPipe: redirectUnauthorizedToLogin }},
  {path: 'account', component: MyAccountPageComponent, canActivate: [AngularFireAuthGuard],  data: { authGuardPipe: redirectUnauthorizedToLogin }},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
