import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatBadgeModule,
  MatButtonModule,
  MatCardModule, MatChipsModule, MatFormFieldModule,
  MatIconModule,
  MatListModule,
  MatSidenavModule, MatSliderModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import { MainToolbarComponent } from './main-toolbar/main-toolbar.component';
import { VotesPageComponent } from './votes-page/votes-page.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { VoteComponent } from './votes-page/vote/vote.component';
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatSortModule} from "@angular/material/sort";
import { VotesListComponent } from './votes-page/votes-list/votes-list.component';
import { NewVoteDialogComponent } from './votes-page/new-vote-dialog/new-vote-dialog.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {MatStepperModule} from "@angular/material/stepper";
import { NewCandidateComponent } from './votes-page/new-vote-dialog/new-candidate/new-candidate.component';
import { CandidatesListComponent } from './votes-page/new-vote-dialog/candidates-list/candidates-list.component';
import {MatTooltipModule} from "@angular/material/tooltip";
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from '../environments/environment';
import { LoginPageComponent } from './login-page/login-page.component';
import {AngularFireAuthGuard} from "@angular/fire/auth-guard";
import {AngularFireAuthModule} from "@angular/fire/auth";
import { MyAccountPageComponent } from './my-account-page/my-account-page.component';
import {NgxMaterialTimepickerModule} from "ngx-material-timepicker";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import { LoadingOverlayComponent } from './loading-overlay/loading-overlay.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import { VoteResultsPageComponent } from './votes-page/vote-results-page/vote-results-page.component';

@NgModule({
  declarations: [
    AppComponent,
    MainToolbarComponent,
    VotesPageComponent,
    VoteComponent,
    VotesListComponent,
    NewVoteDialogComponent,
    NewCandidateComponent,
    CandidatesListComponent,
    LoginPageComponent,
    MyAccountPageComponent,
    LoadingOverlayComponent,
    VoteResultsPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatSliderModule,
    FormsModule,
    MatFormFieldModule,
    MatBadgeModule,
    MatButtonToggleModule,
    MatSortModule,
    MatInputModule,
    MatDialogModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatTooltipModule,
    AngularFirestoreModule.enablePersistence(),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    NgxMaterialTimepickerModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatChipsModule
  ],
  providers: [
    AngularFireAuthGuard
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    NewVoteDialogComponent
  ],
})
export class AppModule {
}
