// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyABKrQWT2q08NJ9zD3FD9erhkWoOvbKnU8",
    authDomain: "lunch-for-today-8f546.firebaseapp.com",
    databaseURL: "https://lunch-for-today-8f546.firebaseio.com",
    projectId: "lunch-for-today-8f546",
    storageBucket: "lunch-for-today-8f546.appspot.com",
    messagingSenderId: "245199946277",
    appId: "1:245199946277:web:e4b3f4935d83978b9675cc",
    measurementId: "G-63T876NLPC"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
